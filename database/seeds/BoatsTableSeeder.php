<?php

use Illuminate\Database\Seeder;

class BoatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 1000; $i++) {
            DB::table('boats')->insert([
                'year'=> rand(1930,2020),
                'make'=> Str::random(10),
                'model'=> Str::random(10),
                'serial'=>Str::random(10),
                'stock_number'=> Str::random(10),
                'equipment_list'=> null,
                'list_price'=> 2500000
            ]);
        }
        for ($i = 1; $i <= 1000; $i++) {
            DB::table('customers')->insert([
                'first_name' => Str::random(10),
                'last_name' => Str::random(10),
                'address' => Str::random(10),
                'email' => Str::random(10)."@gmail.com",
                'phone_number' => Str::random(10),
            ]);
        }
    }
}
