<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!empty($request->key)){
            $customers = Customer::where('first_name','like', '%'.$request->key.'%')->orWhere('last_name','like', '%'.$request->key.'%')->orWhere('address','like', '%'.$request->key.'%')->paginate(15);
        }
        else{
            $customers = Customer::paginate(15);
        }

        return view('customers', [
            'customers' => $customers->appends(Input::except('page')),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request){
        if(!empty($request->first_name ) && !empty($request->last_name) && !empty($request->last_name)){
            $customers = Customer::where('first_name','like', '%'.$request->first_name.'%')->where('last_name','like', '%'.$request->last_name.'%')->where('address','like', '%'.$request->address.'%')->paginate(15);
            return view('customers', [
                'customers' => $customers->appends(Input::except('page')),
            ]);
        }
        if(!empty($request->first_name ) && !empty($request->last_name )){
            $customers = Customer::where('first_name','like', '%'.$request->first_name.'%')->where('last_name','like', '%'.$request->last_name.'%')->paginate(15);
            return view('customers', [
                'customers' => $customers->appends(Input::except('page')),
            ]);
        }



    }
}
