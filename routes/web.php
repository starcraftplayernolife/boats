<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/boats', 'BoatController@index')->name('boat.index');

Route::get('/sales/{stock_number}', 'SaleController@index')->name('sale.index');

Route::get('/customers', 'CustomerController@index')->name('customer.index');

Route::get('/customer/create', 'CustomerController@create')->name('customer.create');

Route::get('/customers/search', 'CustomerController@search')->name('customer.search');