@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard | Boat Inventory</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="get" action="{{ route('boat.index') }}">
                        <div class="form-group">
                            @csrf
                            <label for="name">Search Filter (searches stock_number, make, and model):</label>
                            <input type="text" class="form-control" name="key"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Search / Clear</button>
                    </form>
                    <table>
                        <tr>
                            <th>
                                Make
                            </th>
                            <th>
                                Model
                            </th>
                            <th>
                                Year
                            </th>
                            <th>
                                Stock Number
                            </th>
                            <th>
                                Sale Data
                            </th>
                        </tr>
                        @foreach($boats as $boat)
                        <tr>
                            <td>{{$boat->make}}</td>
                            <td>{{$boat->model}}</td>
                            <td>{{$boat->year}}</td>
                            <td>{{$boat->stock_number}}</td>
                            <td><a href="/sales/{{$boat->stock_number}}">Sales/Sales Info</a></td>
                        </tr>
                        @endforeach
                    </table>
                    {{ $boats->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
