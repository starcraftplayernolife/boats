@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard | Customers</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="get" action="{{ route('customer.index') }}">
                        <div class="form-group">
                            @csrf
                            <label for="name">Search Filter (searches name or address):</label>
                            <input type="text" class="form-control" name="key"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Search / Clear</button>
                    </form>
                    <table>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th>
                                First Name
                            </th>
                            <th>
                                Last Name
                            </th>
                            <th>
                                Address
                            </th>
                            <th>
                                Sales
                            </th>
                        </tr>
                        @foreach($customers as $customer)
                        <tr>
                            <td>{{$customer->id}}</td>
                            <td>{{$customer->first_name}}</td>
                            <td>{{$customer->last_name}}</td>
                            <td>{{$customer->address}}</td>
                            <td><a href="/customer_history/{{$customer->id}}">Sales History</a></td>
                        </tr>
                        @endforeach
                    </table>
                    {{ $customers->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
