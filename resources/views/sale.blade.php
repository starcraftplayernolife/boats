@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard | Sale</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h2>Create new sale, stock number {{$stock_number}}</h2>
                    <form method="get" action="{{ route('customer.create') }}">
                        <div class="form-group">
                            @csrf
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name"/>
                            <label for="first_name">Last Name</label>
                            <input type="text" class="form-control" name="last_name"/>
                            <label for="first_name">Address</label>
                            <input type="text" class="form-control" name="address"/>
                            <label for="first_name">Existing User ID</label>
                            <input type="text" class="form-control" name="user_id"/> 
                            <label for="first_name">Status</label>
                            <select name="status">
                                <option value="quoted">Quoted</option>
                                <option value="pending">Pending</option>
                                <option value="delivered">Delivered</option>
                                <option value="completed">Completed</option>
                            </select>                          
                        </div>
                        <button type="submit" class="btn btn-primary">Insert Sale</button>
                    </form>
                    <h2>Search for customer (Opens a new tab)</h2>
                    <form method="get" target ="_blank"action="{{ route('customer.index') }}">
  
                        <button type="submit" class="btn btn-primary">Open Customers</button>
                    </form>
                    {{-- For later on listing existing sales or customers --}}
                    {{-- @foreach ($sales as $sale)
                        {{$sale->stock_number}}
                    @endforeach
                    @foreach ($customers as $customer)
                        
                    @endforeach --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
